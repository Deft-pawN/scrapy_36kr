import requests


def get_translate(text):
    data = {
        'q': text,
        'target': 'en',
        'key': 'AIzaSyC1bxoX20MvtuJZu2TYiTGxf-mjksfsz_4'
    }

    response = requests.post('https://translation.googleapis.com/language/translate/v2', data=data)
    result = response.json()
    if 'data' in result:
        return response.json()['data']['translations'][0]['translatedText']
    return text
