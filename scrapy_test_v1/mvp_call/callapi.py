import requests
import json


class GetOuterApi:
    def gettoken():
        data = {'username': 'dev@redpulse.com', 'password': 'mvp4ever'}
        result = requests.post("http://192.168.1.196:8000/api/token-auth/", data=data)
        return result.json()

    def getmvpresult(text):
        result_accesstoken = '31b97ee278064dcd8849175e1dd46e5ec29bcca7'
        headers = {'Authorization': 'Token ' + result_accesstoken}
        response = requests.post("http://192.168.1.196:8000/api/v1/scrapyfeed/", data={'contents': json.dumps(text)},
                                 headers=headers)
        print(response.__dict__)

    def getcalaisresult(text):
        open_calais_url = 'https://api.thomsonreuters.com/permid/calais'
        headers = {
            'X-AG-Access-Token': '4reAFg0Azd40DrFa8GAPpi8AA4bVyrMP',
            'Content-Type': 'text/raw',
            'outputformat': 'application/json'
        }
        response = requests.post(
            open_calais_url, data=text.encode('utf-8'),
            headers=headers, timeout=1500)
        return response.content.decode()


if __name__ == '__main__':
    GetOuterApi.getmvpresult()

'''
def getmvpresult(text):
    open_calais_url = 'https://api.thomsonreuters.com/permid/calais'
    headers = {
       'X-AG-Access-Token': '4reAFg0Azd40DrFa8GAPpi8AA4bVyrMP',
       'Content-Type': 'text/raw',
       'outputformat': 'application/json'
   }
    response = requests.post(
        open_calais_url, data=text.encode('utf-8'),
        headers=headers, timeout=1500)
    return response.content.decode()
    # response = requests.post("http://192.168.1.196:8000/api/v1/simple-tagging-readonly/", data={'contents': text}, headers = headers) #use writeonly
'''