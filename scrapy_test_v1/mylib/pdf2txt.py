# -*- coding: utf-8 -*-
"""
    pdf2txt.py
    ~~~~~~~~~

    Created by lhuizhong@gmail.com at 2016-02-21

"""
from pdfminer.pdfinterp import PDFResourceManager, PDFPageInterpreter
from pdfminer.layout import LAParams
from pdfminer.pdfpage import PDFPage
from io import BytesIO
from pdfminer.converter import TextConverter
import sys


def getText(data, pagenumber=None, codec='utf-8'):
    rsrcmgr = PDFResourceManager()
    retstr = BytesIO()
    in_memory_pdf = BytesIO(bytes(data))
    in_memory_pdf.seek(0)
    laparams = LAParams()
    device = TextConverter(rsrcmgr, retstr, codec=codec, laparams=laparams)
    fp = in_memory_pdf
    interpreter = PDFPageInterpreter(rsrcmgr, device)
    password = ""
    maxpages = 10
    caching = True
    pagenos=set()
    if not (pagenumber is None):
        pagenos = (pagenumber,)
    for page in PDFPage.get_pages(fp, pagenos, maxpages=maxpages, password=password,caching=caching, check_extractable=True):
        interpreter.process_page(page)
    fp.close()
    device.close()
    str = retstr.getvalue()
    retstr.close()
    return str

"""
def getText(data, pagenumber=None, codec='utf-8'):
    path = "1204823584.pdf"
    pdf = PyPDF2.PdfFileReader(open(path, "rb"))
    fp = open(path, 'rb')
    num_of_pages = pdf.getNumPages()
    for i in range(num_of_pages):
        inside = [i]
        pagenos = set(inside)
        rsrcmgr = PDFResourceManager()
        retstr = StringIO()
        codec = 'utf-8'
        laparams = LAParams()
        device = TextConverter(rsrcmgr, retstr, codec=codec, laparams=laparams)
        interpreter = PDFPageInterpreter(rsrcmgr, device)
        password = ""
        maxpages = 0
        caching = True
        text = ""
        for page in PDFPage.get_pages(fp, pagenos, maxpages=maxpages, password=password, caching=caching,
                                      check_extractable=True):
            interpreter.process_page(page)
            text = retstr.getvalue()
            text = text.decode("ascii", "replace")
            if re.search(r"to be held (at|on)", text.lower()):
                extract = extract + text + "\n"
                continue

"""

if __name__  == '__main__':
    if len(sys.argv)<2:
        print('Usage: pdf2txt.py filename')
    else:
        fp = open("1204823584.pdf", 'rb')
        print(getText(fp.read(), codec='gbk').decode("utf-8"))
        fp.close()
