# -*- coding: utf-8 -*-

# Define your item pipelines here
#
# Don't forget to add your pipeline to the ITEM_PIPELINES setting
# See: https://doc.scrapy.org/en/latest/topics/item-pipeline.html
import codecs
import json
from google_translation import translation
from mvp_call import callapi
import os


class ScrapyTestV1Pipeline(object):
        @classmethod
        def from_crawler(cls, crawler):
            settings = crawler.settings
            mvpconnect = settings.get('mvpconnect')
            alaisconnect = settings.get('alaisconnect')
            return cls(mvpconnect, alaisconnect)

        def __init__(self, mvpconnect, alaisconnect):
            _mvpconnect = mvpconnect
            _alaisconnect = alaisconnect
            self.confirm_flg = _mvpconnect
            self.alais_flg = _alaisconnect

        def process_item(self, item, spider):
            # line = json.dumps(dict(item), ensure_ascii=False) + "\n"
            line_en = translation.get_translate(item['content'])
            title_en = translation.get_translate(item['title'])
            title_latin = title_en.replace('&#39;s', "'s").replace('&quot;', "'").replace('&#39;', "'").encode(
                "latin-1", 'ignore').decode("latin-1", 'ignore')
            pwd = '/Users/sam/PycharmProjects/scrapy_test_v1/scrapy_test_v1'
            father_path = os.path.abspath(os.path.dirname(pwd) + os.path.sep + "."+'/store/'+str(item['articleID']))
            if not os.path.exists(father_path):
                os.makedirs(father_path)
            else:
                os.chdir(father_path)
            with open(str(item['articleID']) +'_en.txt', 'wb') as f:
                f.write(line_en.encode('utf-8'))

            line_latin = line_en.replace('&#39;s', "'s").replace('&quot;', "'").replace('&#39;', "'").encode(
                "latin-1", 'ignore').decode("latin-1", 'ignore')
            if self.confirm_flg == "True":
                print("*" * 20 + ' Connect to Open calais' + "*" * 20)
                mvp_result_tags = callapi.GetOuterApi.getcalaisresult(line_latin)
                with open(str(item['articleID']) + '.json', 'wb') as f:
                    f.write(mvp_result_tags.encode('utf-8'))
            else:
                print("*" * 20 + 'NO Connect to Open calais' + "*" * 20)
            if self.alais_flg =="True":
                summary_json = {'ID': item['articleID'], 'publish_time': item['publish_time'],
                                'url_direct': item['url_direct'], 'adjunctUrl': item['adjunctUrl'],
                                'tag_result': mvp_result_tags, 'title_cn': item['title'], 'socket_id': item['secCode'],
                                'title_en': title_latin, 'company_name': item['secName']}
                _ = callapi.GetOuterApi.getmvpresult(summary_json)
            else:
                print("*" * 20 + ' NO Connect to RedPulse Mvp ' + "*" * 20)
            os.chdir(pwd)
            return item


