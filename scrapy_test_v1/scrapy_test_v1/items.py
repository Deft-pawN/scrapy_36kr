# -*- coding: utf-8 -*-

# Define here the models for your scraped items
#
# See documentation in:
# https://doc.scrapy.org/en/latest/topics/items.html

import scrapy
from scrapy.item import Field


class ScrapyTestV1Item(scrapy.Item):
    # define the fields for your item here like:
    # name = scrapy.Field()
    websiteName = Field()
    articleID = Field()
    Category = Field()
    title = Field()
    publish_time = Field()
    url = Field()
    content = Field()
    secCode = Field()
    secName = Field()
    scrape_time = Field()
    url_direct = Field()
    adjunctUrl = Field()

