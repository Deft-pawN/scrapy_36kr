import scrapy.spiders
from scrapy.http import Request, FormRequest
import json
from scrapy_test_v1.items import ScrapyTestV1Item
import logging
import datetime
from mylib import pdf2txt
import os


class CninFoSpider(scrapy.Spider):
    name = 'cninfo'
    download_delay = 2
    start_urls = [
        'http://www.cninfo.com.cn/cninfo-new/disclosure/szse',
    ]
    cookie_num = 0
    sitename = 'cninfo'

    def __init__(self, mvpconnect ='', seDate ='', **kwargs):
        super().__init__(**kwargs)
        self.mvpconnect = mvpconnect
        self.seDate = seDate

    def parse(self, response):
        page = 1
        self.cookie_num += 1
        print(self.seDate)
        if page <= 3:
            r = FormRequest.from_response(response, formname='AnnoucementsQueryForm', formdata={'pageNum': str(page)}, meta={'page': page, 'response': response, 'cookiejar': self.cookie_num}, callback=self.parse_search)
            yield r.replace(url='http://www.cninfo.com.cn/cninfo-new/disclosure/szse_latest')   # added: szse
        else:
            print('the page is larger than 3')

    def parse_search(self, response):
        j = json.loads(response.body)
        for dd in j['classifiedAnnouncements'][:2]: # get the largest list
            for d in dd:
                if d['adjunctUrl'][10:20] >= datetime.datetime.utcnow().strftime("%Y-%m-%d"):
                    item = ScrapyTestV1Item()
                    item['websiteName'] = self.sitename
                    item['articleID'] = int(d['announcementId'])
                    item['Category'] = d['announcementTypeName']
                    dt = datetime.datetime.fromtimestamp(d['announcementTime']/1000)
                    item['publish_time'] = dt.strftime('%Y-%m-%d %H:%M')
                    item['title'] = d['announcementTitle']
                    item['secCode'] = d.get('secCode', '')
                    item['secName'] = d.get('secName', '')
                    item['url_direct'] = 'http://www.cninfo.com.cn/cninfo-new/disclosure/szse/download/' + d['announcementId']
                    item['adjunctUrl'] = d['adjunctUrl']
                    self.cookie_num += 1
                    yield Request('http://www.cninfo.com.cn/cninfo-new/disclosure/szse/download/' + d['announcementId'], meta={'item': item, 'cookiejar': self.cookie_num}, callback=self.parse_pdf)
        if j['hasMore']:
            page = response.meta['page'] + 1
            self.cookie_num += 1
            r = FormRequest.from_response(response.meta['response'], formname='AnnoucementsQueryForm',
                                          formdata={'pageNum': str(page)},
                                          meta={'page': page, 'response': response.meta['response'],
                                                'cookiejar': self.cookie_num}, callback=self.parse_search)
            logging.info('!!! Crawling page ' + str(page))
            yield r.replace(url='http://www.cninfo.com.cn/cninfo-new/disclosure/szse_latest')  # added: szse

    def parse_pdf(self, response):
        item = response.meta['item']
        pwd = '/Users/sam/PycharmProjects/scrapy_test_v1/scrapy_test_v1'
        father_path = os.path.abspath(os.path.dirname(pwd) + os.path.sep + "." + '/store/' + str(item['articleID']))
        test_content = pdf2txt.getText(response.body)
        my_json = test_content.decode('utf-8').replace('\n', '').replace('\r', '')
        if not os.path.exists(father_path):
            os.makedirs(father_path)
            os.chdir(father_path)
        else:
            os.chdir(father_path)
        with open(str(item['articleID']) + '.pdf', 'wb') as f:
            f.write(response.body)
        with open(str(item['articleID'])+'_cn.txt', 'wb') as f:
            f.write(test_content)
        os.chdir(pwd)
        item['content'] = my_json
        # item['scrape_time'] = datetime.datetime.now().strftime('%Y-%m-%d %H:%M:%S')
        return item