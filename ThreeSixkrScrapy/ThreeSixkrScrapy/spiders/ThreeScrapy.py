from items import ThreesixkrscrapyItem, ThreeSixKrItemLoader
import scrapy


class A36krSpider(scrapy.Spider):
    name = "36kr"
    start_urls = [
        'https://36kr.com/p/5049025.html',
    ]

    def parse(self, response):
        loader = ThreeSixKrItemLoader(response=response)
        title = response.xpath('//*[@id="J_post_wrapper_5049025"]/div[1]/h1/text()').extract()
        loader.add_value('article_title', title)
        timestamp = response.xpath('//*[@id="J_post_wrapper_5049025"]/div[1]/div/div[1]/div/span[1]/abbr/text()').extract()
        loader.add_value('article_time', timestamp)
        summary = response.xpath('//*[@id="J_post_wrapper_5049025"]/div[1]/div/section[1]/text()').extract()
        loader.add_value('article_summary', summary)
        author = response.xpath('//*[@id="J_post_wrapper_5049025"]/div[1]/div/div[1]/div/a/span/text()').extract()
        loader.add_value('article_author', author)
        content_xpath = response.xpath('//*[@id="J_post_wrapper_5049025"]/div[1]/div/div[2]/section[1]/p')
        content = ''
        for item in range(len(content_xpath)-1):
            content += (response.xpath('//*[@id="J_post_wrapper_5049025"]/div[1]/div/div[2]/section[1]/p['+str(item+2)+']/text()').extract()[0])
        loader.add_value('article_content', content)
        yield loader.load_item()