# -*- coding: utf-8 -*-

# Define here the models for your scraped items
#
# See documentation in:
# https://doc.scrapy.org/en/latest/topics/items.html

import scrapy
from scrapy.contrib.loader import ItemLoader
from scrapy.contrib.loader.processor import TakeFirst, MapCompose


class ThreesixkrscrapyItem(scrapy.Item):
    # define the fields for your item here like:
    # name = scrapy.Field()
    article_title = scrapy.Field()
    article_author = scrapy.Field()
    article_src = scrapy.Field()
    article_url = scrapy.Field()
    article_type = scrapy.Field()
    article_content = scrapy.Field()
    article_summary = scrapy.Field()
    article_icon = scrapy.Field()
    article_time = scrapy.Field()


class ThreeSixKrItemLoader(ItemLoader):
    default_item_class = ThreesixkrscrapyItem
    default_output_processor = TakeFirst()