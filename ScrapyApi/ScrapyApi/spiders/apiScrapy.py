import scrapy
import json
from items import TemplateInfoItem, UserInfoItem ,TemplateInfoItemLoader, UserInfoItemLoader, ScrapyApiItemLoader


class A36krSpider(scrapy.Spider):
    name = "36krapi"
    start_urls = [
        'http://36kr.com/api/search-column/mainsite?per_page=20&page=1',
    ]

    def parse(self, response):
        js = json.loads(response.body)
        for index in range(len(js['data']['items'])):
            loader = ScrapyApiItemLoader(response=response)
            loader.add_value('id', js['data']['items'][index]['id'])
            loader.add_value('column_id', js['data']['items'][index]['column_id'])
            loader.add_value('column_name', js['data']['items'][index]['column_name'])
            loader.add_value('tag_id', js['data']['items'][index]['tag_id'])
            loader.add_value('published_at', js['data']['items'][index]['published_at'])
            loader.add_value('summary', js['data']['items'][index]['summary'])
            loader.add_value('title', js['data']['items'][index]['title'])
            # loader.add_value('web_cover',  js['data']['items'][index]['web_cover'])
            loader.add_value('user_id', js['data']['items'][index]['user_id'])
            loader.add_value('highlight', js['data']['items'][index]['highlight'])
            loader.add_value('_type', js['data']['items'][index]['_type'])
            loader.add_value('highlight', js['data']['items'][index]['highlight'])
            loader.add_value('template_info', js['data']['items'][index]['template_info'])
            loader.add_value('user_info', js['data']['items'][index]['user_info'])
            yield loader.load_item()
