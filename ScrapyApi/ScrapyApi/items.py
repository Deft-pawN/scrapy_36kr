# -*- coding: utf-8 -*-

# Define here the models for your scraped items
#
# See documentation in:
# https://doc.scrapy.org/en/latest/topics/items.html

import scrapy
from scrapy.contrib.loader import ItemLoader
from scrapy.contrib.loader.processor import TakeFirst, MapCompose
from w3lib.html import remove_tags
from scrapy.loader.processors import Identity


class ScrapyApiItem(scrapy.Item):
    # define the fields for your item here like:
    # name = scrapy.Field()
    id = scrapy.Field()
    column_id = scrapy.Field()
    column_name = scrapy.Field()
    project_id = scrapy.Field()
    tag_id = scrapy.Field()
    title = scrapy.Field()
    cover = scrapy.Field()
    template_info = scrapy.Field(input_processor= Identity())
    published_at = scrapy.Field()
    extraction_tags = scrapy.Field(output_processor= Identity())
    summary = scrapy.Field()
    web_cover = scrapy.Field()
    user_id = scrapy.Field()
    user_info = scrapy.Field(input_processor= Identity())
    highlight = scrapy.Field()
    _type = scrapy.Field()
    _score = scrapy.Field()


class TemplateInfoItem(scrapy.Item):
    # define the fields for your item here like:
    # name = scrapy.Field()
    template_type = scrapy.Field()
    template_title = scrapy.Field()
    template_title_isSame = scrapy.Field()
    template_cover = scrapy.Field()


class UserInfoItem(scrapy.Item):
    # define the fields for your item here like:
    # name = scrapy.Field()
    id = scrapy.Field()
    avatar_url = scrapy.Field()
    name = scrapy.Field()
    nickname = scrapy.Field()
    realname = scrapy.Field()
    email = scrapy.Field()


class ScrapyApiItemLoader(ItemLoader):
    default_item_class = ScrapyApiItem
    default_output_processor = TakeFirst()


class TemplateInfoItemLoader(ItemLoader):
    default_item_class = TemplateInfoItem
    default_output_processor = TakeFirst()


class UserInfoItemLoader(ItemLoader):
    default_item_class = UserInfoItem
    default_output_processor = TakeFirst()


